<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('adminLTE.master');
});

// Route::get('/films/create', [FilmController::class, 'create']);
// Route::post('/films', [FilmController::class, 'store']);
// Route::get('/films', [FilmController::class, 'index'])->name("films.index");
// Route::get('/films/{id}', [FilmController::class, 'show']);
// Route::get('films/{id}/edit', [FilmController::class, 'edit']);
// Route::put('films/{id}', [FilmController::class, 'update']);
// Route::delete('films/{id}', [FilmController::class, 'destroy']);

Route::resource('films', FilmController::class);
Route::resource('photos', PhotoController::class);

